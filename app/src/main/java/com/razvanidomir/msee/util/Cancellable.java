package com.razvanidomir.msee.util;

public interface Cancellable {
    void cancel();
}
