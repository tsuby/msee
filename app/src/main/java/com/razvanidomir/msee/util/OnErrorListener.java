package com.razvanidomir.msee.util;

public interface OnErrorListener {
    void onError(Exception e);
}
