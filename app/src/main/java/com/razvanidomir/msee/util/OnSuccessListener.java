package com.razvanidomir.msee.util;

public interface OnSuccessListener<E> {
    void onSuccess(E e);
}
