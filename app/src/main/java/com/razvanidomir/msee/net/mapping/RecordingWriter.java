package com.razvanidomir.msee.net.mapping;

import android.util.JsonWriter;

import com.razvanidomir.msee.content.Recording;

import java.io.IOException;

import static com.razvanidomir.msee.net.mapping.Api.Recording.DETAILS;
import static com.razvanidomir.msee.net.mapping.Api.Recording.TITLE;
import static com.razvanidomir.msee.net.mapping.Api.Recording.UPDATED;
import static com.razvanidomir.msee.net.mapping.Api.Recording.USER_ID;
import static com.razvanidomir.msee.net.mapping.Api.Recording.VERSION;
import static com.razvanidomir.msee.net.mapping.Api.Recording._ID;

public class RecordingWriter implements ResourceWriter<Recording, JsonWriter> {
    @Override
    public void write(Recording note, JsonWriter writer) throws IOException {
        writer.beginObject();
        {
            if (note.getmId() != null) {
                writer.name(_ID).value(note.getmId());
            }
            writer.name(TITLE).value(note.getmTitle());
            writer.name(DETAILS).value(note.getmDetails());
            if (note.getmUpdated() > 0) {
                writer.name(UPDATED).value(note.getmUpdated());
            }
            if (note.getmUserId() != null) {
                writer.name(USER_ID).value(note.getmUserId());
            }
            if (note.getmVersion() > 0) {
                writer.name(VERSION).value(note.getmVersion());
            }
        }
        writer.endObject();
    }
}
