package com.razvanidomir.msee.net.mapping;

import com.razvanidomir.msee.content.Recording;

import org.json.JSONException;
import org.json.JSONObject;

import static com.razvanidomir.msee.net.mapping.Api.Recording.DETAILS;
import static com.razvanidomir.msee.net.mapping.Api.Recording.TITLE;
import static com.razvanidomir.msee.net.mapping.Api.Recording.UPDATED;
import static com.razvanidomir.msee.net.mapping.Api.Recording.USER_ID;
import static com.razvanidomir.msee.net.mapping.Api.Recording.VERSION;
import static com.razvanidomir.msee.net.mapping.Api.Recording._ID;

/**
 * @author Razvan Idomir
 */

public class RecordingJsonObjectReader implements ResourceReader<Recording, JSONObject> {
    private static final String TAG = RecordingJsonObjectReader.class.getSimpleName();

    @Override
    public Recording read(JSONObject obj) throws JSONException {
        Recording recording = new Recording();
        recording.setmId(obj.getString(_ID));
        recording.setmTitle(obj.getString(TITLE));
        recording.setmDetails(obj.getString(DETAILS));
        recording.setmUpdated(obj.getLong(UPDATED));
        recording.setmVersion(obj.getInt(VERSION));
        recording.setmUserId(obj.getString(USER_ID));
        return recording;
    }
}
