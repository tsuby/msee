package com.razvanidomir.msee.net.mapping;

import android.util.JsonReader;

import org.json.JSONException;

import java.io.IOException;

public interface ResourceReader<T, Reader> {
    T read(Reader reader) throws IOException, JSONException, Exception;
}
