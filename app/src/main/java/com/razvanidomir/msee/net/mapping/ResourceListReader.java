package com.razvanidomir.msee.net.mapping;

import android.util.JsonReader;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ResourceListReader<E> implements ResourceReader<List<E>, JsonReader> {

    private final ResourceReader<E, JsonReader> mResourceReader;

    public ResourceListReader(ResourceReader<E, JsonReader> mResourceReader) {
        this.mResourceReader = mResourceReader;
    }

    @Override
    public List<E> read(JsonReader reader) throws Exception {
        List<E> entityList = new ArrayList<E>();
        reader.beginArray();
        while(reader.hasNext()) {
            entityList.add(mResourceReader.read(reader));
        }
        reader.endArray();
        return entityList;
    }
}
