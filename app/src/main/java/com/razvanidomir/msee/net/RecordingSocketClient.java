package com.razvanidomir.msee.net;

import android.content.Context;
import android.util.Log;

import com.razvanidomir.msee.R;
import com.razvanidomir.msee.content.Recording;
import com.razvanidomir.msee.net.mapping.IdJsonObjectReader;
import com.razvanidomir.msee.net.mapping.RecordingJsonObjectReader;

import org.json.JSONObject;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.razvanidomir.msee.net.mapping.Api.Recording.RECORDING_CREATED;
import static com.razvanidomir.msee.net.mapping.Api.Recording.RECORDING_DELETED;
import static com.razvanidomir.msee.net.mapping.Api.Recording.RECORDING_UPDATED;

public class RecordingSocketClient {
    private static final String TAG = RecordingSocketClient.class.getSimpleName();
    private final Context mContext;
    private Socket mSocket;
    private ResourceChangeListener<Recording> mResourceListener;

    public RecordingSocketClient(Context context) {
        mContext = context;
        Log.d(TAG, "created");
    }

    public void subscribe(final ResourceChangeListener<Recording> resourceListener) {
        Log.d(TAG, "subscribe");
        mResourceListener = resourceListener;
        try {
            mSocket = IO.socket(mContext.getString(R.string.api_url));
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "socket connected");
                }
            });
            mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "socket disconnected");
                }
            });
            mSocket.on(RECORDING_CREATED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        Recording recording = new RecordingJsonObjectReader().read((JSONObject) args[0]);
                        Log.d(TAG, String.format("recording created %s", recording.toString()));
                        mResourceListener.onCreated(recording);
                    } catch (Exception e) {
                        Log.w(TAG, "recording created", e);
                        mResourceListener.onError(new ResourceException(e));
                    }
                }
            });
            mSocket.on(RECORDING_UPDATED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        Recording recording = new RecordingJsonObjectReader().read((JSONObject) args[0]);
                        Log.d(TAG, String.format("recording updated %s", recording.toString()));
                        mResourceListener.onUpdated(recording);
                    } catch (Exception e) {
                        Log.w(TAG, "recording updated", e);
                        mResourceListener.onError(new ResourceException(e));
                    }
                }
            });
            mSocket.on(RECORDING_DELETED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        String id = new IdJsonObjectReader().read((JSONObject) args[0]);
                        Log.d(TAG, String.format("recording deleted %s", id));
                        mResourceListener.onDeleted(id);
                    } catch (Exception e) {
                        Log.w(TAG, "recording deleted", e);
                        mResourceListener.onError(new ResourceException(e));
                    }
                }
            });
            mSocket.connect();
        } catch (Exception e) {
            Log.w(TAG, "socket error", e);
            mResourceListener.onError(new ResourceException(e));
        }
    }

    public void unsubscribe() {
        Log.d(TAG, "unsubscribe");
        if (mSocket != null) {
            mSocket.disconnect();
        }
        mResourceListener = null;
    }
}
