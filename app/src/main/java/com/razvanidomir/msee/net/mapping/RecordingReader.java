package com.razvanidomir.msee.net.mapping;

import android.util.JsonReader;
import android.util.Log;

import com.razvanidomir.msee.content.Recording;

import java.io.IOException;

import static com.razvanidomir.msee.net.mapping.Api.Recording.DETAILS;
import static com.razvanidomir.msee.net.mapping.Api.Recording.TITLE;
import static com.razvanidomir.msee.net.mapping.Api.Recording.UPDATED;
import static com.razvanidomir.msee.net.mapping.Api.Recording.USER_ID;
import static com.razvanidomir.msee.net.mapping.Api.Recording.VERSION;
import static com.razvanidomir.msee.net.mapping.Api.Recording._ID;

public class RecordingReader implements ResourceReader<Recording, JsonReader> {
    private static final String TAG = RecordingReader.class.getSimpleName();

    @Override
    public Recording read(JsonReader reader) throws IOException {
        Recording recording = new Recording();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(_ID)) {
                recording.setmId(reader.nextString());
            } else if (name.equals(TITLE)) {
                recording.setmTitle(reader.nextString());
            } else if (name.equals(DETAILS)) {
                recording.setmDetails(reader.nextString());
            } else if (name.equals(UPDATED)) {
                recording.setmUpdated(reader.nextLong());
            } else if (name.equals(USER_ID)) {
                recording.setmUserId(reader.nextString());
            } else if (name.equals(VERSION)) {
                recording.setmVersion(reader.nextInt());
            } else {
                reader.skipValue();
                Log.w(TAG, String.format("Recording property '%s' ignored", name));
            }
        }
        reader.endObject();
        return recording;
    }
}
