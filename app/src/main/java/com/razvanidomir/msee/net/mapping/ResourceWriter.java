package com.razvanidomir.msee.net.mapping;

import android.util.JsonWriter;

import java.io.IOException;

public interface ResourceWriter<T, Writer> {
    void write(T e, Writer writer) throws IOException;
}
