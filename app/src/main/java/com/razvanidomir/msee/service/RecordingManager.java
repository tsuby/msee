package com.razvanidomir.msee.service;

import android.content.Context;
import android.util.Log;

import com.razvanidomir.msee.content.Recording;
import com.razvanidomir.msee.content.User;
import com.razvanidomir.msee.content.database.mseeDatabase;
import com.razvanidomir.msee.net.LastModifiedList;
import com.razvanidomir.msee.net.RecordingRestClient;
import com.razvanidomir.msee.net.RecordingSocketClient;
import com.razvanidomir.msee.net.ResourceChangeListener;
import com.razvanidomir.msee.net.ResourceException;
import com.razvanidomir.msee.util.Cancellable;
import com.razvanidomir.msee.util.CancellableCallable;
import com.razvanidomir.msee.util.OnErrorListener;
import com.razvanidomir.msee.util.OnSuccessListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class RecordingManager extends Observable {
    private static final String TAG = RecordingManager.class.getSimpleName();
    private final mseeDatabase mMD;

    private ConcurrentMap<String, Recording> mRecordings = new ConcurrentHashMap<String, Recording>();
    private String mRecordingsLastUpdate;

    private final Context mContext;
    private RecordingRestClient mRecordingRestClient;
    private RecordingSocketClient mRecordingSocketClient;
    private String mToken;
    private User mCurrentUser;

    public RecordingManager(Context context) {
        mContext = context;
        mMD = new mseeDatabase(context);
    }

    public CancellableCallable<LastModifiedList<Recording>> getRecordingsCall() {
        Log.d(TAG, "getRecordingsCall");
        return mRecordingRestClient.search(mRecordingsLastUpdate);
    }

    public List<Recording> executeRecordingsCall(CancellableCallable<LastModifiedList<Recording>> getRecordingsCall) throws Exception {
        Log.d(TAG, "execute getRecordings...");
        LastModifiedList<Recording> result = getRecordingsCall.call();
        List<Recording> recordings = result.getList();
        if (recordings != null) {
            mRecordingsLastUpdate = result.getLastModified();
            updateCachedRecordings(recordings);
            notifyObservers();
        }
        return cachedRecordingsByUpdated();
    }

    public RecordingLoader getRecordingLoader() {
        Log.d(TAG, "getRecordingLoader...");
        return new RecordingLoader(mContext, this);
    }

    public void setRecordingRestClient(RecordingRestClient recordingRestClient) {
        mRecordingRestClient = recordingRestClient;
    }

    public Cancellable getRecordingsAsync(final OnSuccessListener<List<Recording>> successListener, OnErrorListener errorListener) {
        Log.d(TAG, "getRecordingsAsync...");
        return mRecordingRestClient.searchAsync(mRecordingsLastUpdate, new OnSuccessListener<LastModifiedList<Recording>>() {

            @Override
            public void onSuccess(LastModifiedList<Recording> result) {
                Log.d(TAG, "getRecordingsAsync succeeded");
                List<Recording> recordings = result.getList();
                if (recordings != null) {
                    mRecordingsLastUpdate = result.getLastModified();
                    updateCachedRecordings(recordings);
                }
                successListener.onSuccess(cachedRecordingsByUpdated());
                notifyObservers();
            }
        }, errorListener);
    }

    public Cancellable getRecordingAsync(
            final String recordingId,
            final OnSuccessListener<Recording> successListener,
            final OnErrorListener errorListener) {
        Log.d(TAG, "getRecordingAsync...");
        return mRecordingRestClient.readAsync(recordingId, new OnSuccessListener<Recording>() {

            @Override
            public void onSuccess(Recording recording) {
                Log.d(TAG, "getRecordingAsync succeeded");
                if (recording == null) {
                    setChanged();
                    mRecordings.remove(recordingId);
                } else {
                    if (!recording.equals(mRecordings.get(recording.getmId()))) {
                        setChanged();
                        mRecordings.put(recordingId, recording);
                    }
                }
                successListener.onSuccess(recording);
                notifyObservers();
            }
        }, errorListener);
    }

    public Cancellable saveRecordingAsync(
            final Recording recording,
            final OnSuccessListener<Recording> successListener,
            final OnErrorListener errorListener) {
        Log.d(TAG, "saveRecordingAsync...");
        return mRecordingRestClient.updateAsync(recording, new OnSuccessListener<Recording>() {

            @Override
            public void onSuccess(Recording recording) {
                Log.d(TAG, "saveRecordingAsync succeeded");
                mRecordings.put(recording.getmId(), recording);
                successListener.onSuccess(recording);
                setChanged();
                notifyObservers();
            }
        }, errorListener);
    }

    public void subscribeChangeListener() {
        mRecordingSocketClient.subscribe(new ResourceChangeListener<Recording>() {
            @Override
            public void onCreated(Recording recording) {
                Log.d(TAG, "changeListener, onCreated");
                ensureRecordingCached(recording);
            }

            @Override
            public void onUpdated(Recording recording) {
                Log.d(TAG, "changeListener, onUpdated");
                ensureRecordingCached(recording);
            }

            @Override
            public void onDeleted(String recordingId) {
                Log.d(TAG, "changeListener, onDeleted");
                if (mRecordings.remove(recordingId) != null) {
                    setChanged();
                    notifyObservers();
                }
            }

            private void ensureRecordingCached(Recording recording) {
                if (!recording.equals(mRecordings.get(recording.getmId()))) {
                    Log.d(TAG, "changeListener, cache updated");
                    mRecordings.put(recording.getmId(), recording);
                    setChanged();
                    notifyObservers();
                }
            }

            @Override
            public void onError(Throwable t) {
                Log.e(TAG, "changeListener, error", t);
            }
        });
    }

    public void unsubscribeChangeListener() {
        mRecordingSocketClient.unsubscribe();
    }

    public void setRecordingSocketClient(RecordingSocketClient recordingSocketClient) {
        mRecordingSocketClient = recordingSocketClient;
    }

    private void updateCachedRecordings(List<Recording> recordings) {
        Log.d(TAG, "updateCachedRecordings");
        for (Recording recording : recordings) {
            mRecordings.put(recording.getmId(), recording);
        }
        setChanged();
    }

    private List<Recording> cachedRecordingsByUpdated() {
        ArrayList<Recording> recordings = new ArrayList<>(mRecordings.values());
        Collections.sort(recordings, new RecordingByUpdatedComparator());
        return recordings;
    }

    public List<Recording> getCachedRecordings() {
        return cachedRecordingsByUpdated();
    }

    public Cancellable loginAsync(
            String username, String password,
            final OnSuccessListener<String> successListener,
            final OnErrorListener errorListener) {
        final User user = new User(username, password);
        return mRecordingRestClient.getToken(
                user, new OnSuccessListener<String>() {

                    @Override
                    public void onSuccess(String token) {
                        mToken = token;
                        if (mToken != null) {
                            user.setToken(mToken);
                            setCurrentUser(user);
                            mMD.saveUser(user);
                            successListener.onSuccess(mToken);
                        } else {
                            errorListener.onError(new ResourceException(new IllegalArgumentException("Invalid credentials")));
                        }
                    }
                }, errorListener);
    }

    public void setCurrentUser(User currentUser) {
        mCurrentUser = currentUser;
        mRecordingRestClient.setUser(currentUser);
    }

    public User getCurrentUser() {
        return mMD.getCurrentUser();
    }

    private class RecordingByUpdatedComparator implements java.util.Comparator<Recording> {
        @Override
        public int compare(Recording n1, Recording n2) {
            return (int) (n1.getmUpdated() - n2.getmUpdated());
        }
    }
}