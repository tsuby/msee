package com.razvanidomir.msee;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.razvanidomir.msee.content.Recording;
import com.razvanidomir.msee.dummy.DummyContent;
import com.razvanidomir.msee.dummy.DummyItem;
import com.razvanidomir.msee.util.Cancellable;
import com.razvanidomir.msee.util.DialogUtils;
import com.razvanidomir.msee.util.OnErrorListener;
import com.razvanidomir.msee.util.OnSuccessListener;

import java.util.List;

public class RecordingListActivity extends AppCompatActivity {
    public static final String TAG = RecordingListActivity.class.getSimpleName();

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet device.
     */
    private boolean mTwoPane;

    /**
     * Whether or not the the recordings were loaded.
     */
    private boolean mRecordingsLoaded;

    /**
     * Reference to the singleton app used to access the app state and logic.
     */
    private mseeApp mApp;

    /**
     * Reference to the last async call used for cancellation.
     */
    private Cancellable mGetRecordingsAsyncCall;
    private View mContentLoadingView;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        mApp = (mseeApp) getApplication();
        setContentView(R.layout.activity_recording_list);
        setupToolbar();
        setupFloatingActionBar();
        setupRecyclerView();
        checkTwoPaneMode();
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        startGetRecordingsAsyncCall();
        mApp.getRecordingManager().subscribeChangeListener();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        //ensureGetRecordingsAsyncTaskCancelled();
        ensureGetRecordingsAsyncCallCancelled();
        mApp.getRecordingManager().unsubscribeChangeListener();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    private void setupFloatingActionBar() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void setupRecyclerView() {
        mContentLoadingView = findViewById(R.id.content_loading);
        mRecyclerView = (RecyclerView) findViewById(R.id.recording_list);
    }

    private void checkTwoPaneMode() {
//        if (findViewById(R.id.recording_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
//            mTwoPane = true;
//        }
    }

    private void startGetRecordingsAsyncCall() {
        if (mRecordingsLoaded) {
            Log.d(TAG, "start getRecordingsAsyncCall - content already loaded, return");
            return;
        }
        showLoadingIndicator();
        mGetRecordingsAsyncCall = mApp.getRecordingManager().getRecordingsAsync(
                new OnSuccessListener<List<Recording>>() {
                    @Override
                    public void onSuccess(final List<Recording> recordings) {
                        Log.d(TAG, "getRecordingsAsyncCall - success");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showContent(recordings);
                            }
                        });
                    }
                }, new OnErrorListener() {
                    @Override
                    public void onError(final Exception e) {
                        Log.d(TAG, "getRecordingsAsyncCall - error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showError(e);
                            }
                        });
                    }
                }
        );
    }


    private void ensureGetRecordingsAsyncCallCancelled() {
        if (mGetRecordingsAsyncCall != null) {
            Log.d(TAG, "ensureGetRecordingsAsyncCallCancelled - cancelling the task");
            mGetRecordingsAsyncCall.cancel();
        }
    }

    private void showError(Exception e) {
        Log.e(TAG, "showError", e);
        if (mContentLoadingView.getVisibility() == View.VISIBLE) {
            mContentLoadingView.setVisibility(View.GONE);
        }
        DialogUtils.showError(this, e);
    }

    private void showLoadingIndicator() {
        Log.d(TAG, "showLoadingIndicator");
        mRecyclerView.setVisibility(View.GONE);
        mContentLoadingView.setVisibility(View.VISIBLE);
    }

    private void showContent(final List<Recording> recordings) {
        Log.d(TAG, "showContent");
        mRecyclerView.setAdapter(new RecordingRecyclerViewAdapter(recordings));
        mContentLoadingView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    public class RecordingRecyclerViewAdapter
            extends RecyclerView.Adapter<RecordingRecyclerViewAdapter.ViewHolder> {

        private final List<Recording> mValues;

        public RecordingRecyclerViewAdapter(List<Recording> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recording_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(mValues.get(position).getmId());
            holder.mContentView.setText(mValues.get(position).getmDetails());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Log.d("MINE", "mTwoPane");
                        Bundle arguments = new Bundle();
                        arguments.putString(RecordingDetailFragment.RECORDING_ID, holder.mItem.getmId());
                        RecordingDetailFragment fragment = new RecordingDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.recording_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, RecordingDetailActivity.class);
                        intent.putExtra(RecordingDetailFragment.RECORDING_ID, holder.mItem.getmId());
                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public Recording mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }
}
