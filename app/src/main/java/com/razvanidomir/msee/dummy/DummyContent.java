package com.razvanidomir.msee.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author razvan.idomir
 */

public class DummyContent {
    public static final List<DummyItem> ITEMS = new ArrayList<>();
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<>();

    private static final int COUNT = 30;
    static {
        for(int i = 1; i <= COUNT; ++i) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(DummyItem dummyItem) {
        ITEMS.add(dummyItem);
        ITEM_MAP.put(dummyItem.id, dummyItem);
    }

    private static DummyItem createDummyItem(int position) {
        return new DummyItem(String.valueOf(position), "Item " + position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Recording #");
        builder.append(String.valueOf(position));
        builder.append(" details");
        for(int i = 0; i < 20; ++i) {
            builder.append("More recording details");
        }
        return builder.toString();
    }
}
