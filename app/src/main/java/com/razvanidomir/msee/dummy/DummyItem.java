package com.razvanidomir.msee.dummy;

/**
 * @author razvan.idomir
 */

public class DummyItem {
    public String id;
    public String content;
    public String details;

    public DummyItem(String id, String content, String details) {
        this.id = id;
        this.content = content;
        this.details = details;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
