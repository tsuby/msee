package com.razvanidomir.msee;


import android.app.Application;
import android.util.Log;

import com.razvanidomir.msee.net.RecordingRestClient;
import com.razvanidomir.msee.net.RecordingSocketClient;
import com.razvanidomir.msee.service.RecordingManager;

public class mseeApp extends Application {
    public static final String TAG = mseeApp.class.getSimpleName();
    private RecordingManager mRecordingManager;
    private RecordingRestClient mRecordingRestClient;
    private RecordingSocketClient mRecordingSocketClient;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        mRecordingManager = new RecordingManager(this);
        mRecordingRestClient = new RecordingRestClient(this);
        mRecordingSocketClient = new RecordingSocketClient(this);
        mRecordingManager.setRecordingRestClient(mRecordingRestClient);
        mRecordingManager.setRecordingSocketClient(mRecordingSocketClient);
    }

    public RecordingManager getRecordingManager() {
        return mRecordingManager;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.d(TAG, "onTerminate");
    }
}
