//package com.razvanidomir.msee.widget;
//
//import android.content.Context;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.TextView;
//
//import com.razvanidomir.msee.R;
//import com.razvanidomir.msee.content.Recording;
//
//import java.util.List;
//
//public class RecordingListAdapter extends BaseAdapter {
//    private final String TAG = RecordingListAdapter.class.getSimpleName();
//    private final Context mContext;
//    private List<Recording> mNotes;
//
//    public RecordingListAdapter(Context mContext, List<Recording> mNotes) {
//        this.mContext = mContext;
//        this.mNotes = mNotes;
//    }
//
//    @Override
//    public int getCount() {
//        return mNotes.size();
//    }
//
//    @Override
//    public Object getItem(int i) {
//        return mNotes.get(i);
//    }
//
//    @Override
//    public long getItemId(int i) {
//        return Long.parseLong(mNotes.get(i).getmId());
//    }
//
//    @Override
//    public View getView(int position, View view, ViewGroup viewGroup) {
//        View recordingLayout = LayoutInflater.from(mContext).inflate(R.layout.recording_detail, null);
//        ((TextView) recordingLayout.findViewById(R.id.recording_detail)).setText(mNotes.get(position).getmTitle());
//        Log.d(TAG, "getView " + position);
//        return recordingLayout;
//    }
//
//    public void refresh() {
//        notifyDataSetChanged();
//    }
//}
