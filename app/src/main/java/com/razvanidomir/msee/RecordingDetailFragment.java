package com.razvanidomir.msee;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.razvanidomir.msee.content.Recording;
import com.razvanidomir.msee.util.Cancellable;
import com.razvanidomir.msee.util.DialogUtils;
import com.razvanidomir.msee.util.OnErrorListener;
import com.razvanidomir.msee.util.OnSuccessListener;

public class RecordingDetailFragment extends Fragment {
    public static final String TAG = RecordingDetailFragment.class.getSimpleName();

    /**
     * The fragment argument representing the item ID that this fragment represents.
     */
    public static final String RECORDING_ID = "recording_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Recording mRecording;

    private mseeApp mApp;

    private Cancellable mFetchRecordingAsync;
    private TextView mRecordingTextView;
    private CollapsingToolbarLayout mAppBarLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RecordingDetailFragment() {
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mApp = (mseeApp) context.getApplicationContext();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(RECORDING_ID)) {
            // In a real-world scenario, use a Loader
            // to load content from a content provider.
            Activity activity = this.getActivity();
            mAppBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.recording_detail, container, false);
        mRecordingTextView = (TextView) rootView.findViewById(R.id.recording_text);
        fillRecordingDetails();
        fetchRecordingAsync();
        return rootView;
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
    }

    private void fetchRecordingAsync() {
        mFetchRecordingAsync = mApp.getRecordingManager().getRecordingAsync(
                getArguments().getString(RECORDING_ID),
                new OnSuccessListener<Recording>() {

                    @Override
                    public void onSuccess(final Recording recording) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mRecording = recording;
                                fillRecordingDetails();
                            }
                        });
                    }
                }, new OnErrorListener() {

                    @Override
                    public void onError(final Exception e) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.showError(getActivity(), e);
                            }
                        });
                    }
                });
    }

    private void fillRecordingDetails() {
        if (mRecording != null) {
            if (mAppBarLayout != null) {
                mAppBarLayout.setTitle(mRecording.getmTitle());
            }
            mRecordingTextView.setText(mRecording.getmDetails());
        }
    }
}
