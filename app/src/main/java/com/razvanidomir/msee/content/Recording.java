package com.razvanidomir.msee.content;

/**
 * @author Razvan Idomir
 */

public class Recording {
    private String mId;
    private String mUserId;
    private String mTitle;
    private String mDetails;
    private long mUpdated;
    private int mVersion;

    public Recording() {}

    public Recording(String id, String title, String details) {
        mId = id;
        mTitle = title;
        mDetails = details;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDetails() {
        return mDetails;
    }

    public void setmDetails(String mDetails) {
        this.mDetails = mDetails;
    }

    public int getmVersion() {
        return mVersion;
    }

    public void setmVersion(int mVersion) {
        this.mVersion = mVersion;
    }

    public long getmUpdated() {
        return mUpdated;
    }

    public void setmUpdated(long mUpdated) {
        this.mUpdated = mUpdated;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }
}
